'use strict';

module.exports = exec;

const path = require('path')
const childProcess = require('child_process')
const Package = require('@baseh-cli-dev/package')
const log = require('@baseh-cli-dev/log')

const SETTINGS = {
  init: '@baseh-cli-dev/init'
}

const CACHE_DIR = 'dependencies'

async function exec () {
  let targetPath = process.env.CLI_TARGET_PATH
  let storeDir = ''
  let pkg
  const homePath = process.env.CLI_HOME_PATH
  log.verbose('targetPath', targetPath)
  log.verbose('homePath', homePath)
  const cmdObj = arguments[arguments.length - 1]
  const cmdName = cmdObj.name()
  const packageName = SETTINGS[cmdName]
  const packageVersion = 'latest'

  if (!targetPath) {
    targetPath = path.resolve(homePath, CACHE_DIR) // 生成缓存路径
    storeDir = path.resolve(targetPath, 'node_modules')
    log.verbose('targetPath', targetPath)
    log.verbose('homePath', homePath)
    pkg = new Package({
      targetPath,
      storeDir,
      packageName,
      packageVersion
    })
    if (await pkg.exists()) {
      // 更新
      await pkg.update()
    } else {
      // 安装
      await pkg.install()
    }
  } else {
    pkg = new Package({
      targetPath,
      packageName,
      packageVersion
    })
    const rootFilePath = pkg.getRootFilePath()
    if (rootFilePath) {
      // require(rootFilePath).apply(null, arguments)
      try {
        // require(rootFilePath).call(null, Array.from(arguments))

        // 在node子进程中调用
        // childProcess.fork(rootFilePath)
        const args = Array.from(arguments)
        const cmd = args[args.length - 1]
        const o = Object.create(null)
        Object.keys(cmd).forEach(key => {
          if (cmd.hasOwnProperty(key) &&
            !key.startsWith('_') &&
            key !== 'parent'
          ) {
            o[key] = cmd[key]
          }
        })
        args[args.length - 1] = o
        const code = `require('${rootFilePath}').call(null, ${JSON.stringify(args)})`
        // const child = childProcess.spawn('node', ['-e', code], {
        const child = spawn('node', ['-e', code], {
          cwd: process.cwd(),
          stdio: 'inherit'
        })
        child.on('error', (e) => {
          log.error(e.message)
        })
        child.on('exit', (e) => {
          log.verbose('命令执行成功', e)
          process.exit(e)
        })
        // child.stdout.on('data', (chunk => {
        //
        // }))
        // child.stderr.on('data', (chunk => {
        //
        // }))
      } catch(e) {
        log.error(e.message)
      }
    }
  }
  //
  // console.log('exec')
  // console.log(pkg.getRootFilePath())
  // console.log(process.env.CLI_TARGET_PATH)
}

function spawn (command, args, options) {
  const win32 = process.platform === 'win32'
  const cmd = win32 ? 'cmd' : command
  const cmdArgs = win32 ? ['/c'].concat(command, args) : args

  return childProcess.spawn(cmd, cmdArgs, options || {})
}