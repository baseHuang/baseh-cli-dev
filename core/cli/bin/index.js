#! /usr/bin/env node

// const utils = require('@baseh-cli-dev/utils')
// console.log(utils())
// console.log('hello baseh-cli-dev11122222')
const importLocal = require("import-local")
if (importLocal(__filename)) {
  require('npmlog').info('cli', '正在使用本地版本')
} else {
  require('../lib')(process.argv.slice(2))
}