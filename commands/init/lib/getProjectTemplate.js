const request = require('@baseh-cli-dev/request')

module.exports = function () {
  return request({
    url: 'project/template'
  })
}